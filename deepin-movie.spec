%undefine __cmake_in_source_build
%global _lto_cflags %{nil}

Name:           deepin-movie
Version:        5.7.11
Release:        1
Summary:        Deepin movie based on mpv
Summary(zh_CN): 深度影音
License:        GPLv3
URL:            https://github.com/linuxdeepin/deepin-movie-reborn
Source0:        %{url}/archive/%{version}/%{name}-%{version}.tar.gz
Source1:        %{name}-appdata.xml
Patch0:         fix_linking.patch

BuildRequires:  pkgconfig(Qt5Concurrent)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  cmake(Qt5LinguistTools)
BuildRequires:  pkgconfig(Qt5Network)
BuildRequires:  pkgconfig(Qt5Sql)
BuildRequires:  pkgconfig(Qt5Svg)
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(Qt5X11Extras)
BuildRequires:  dtkcore-devel
BuildRequires:  dtkwidget-devel
BuildRequires:  pkgconfig(dvdnav)
BuildRequires:  pkgconfig(gsettings-qt)
BuildRequires:  pkgconfig(libffmpegthumbnailer)
BuildRequires:  pkgconfig(libavformat)
BuildRequires:  pkgconfig(libavutil)
BuildRequires:  pkgconfig(libavcodec)
BuildRequires:  pkgconfig(libavresample)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libpulse-simple)
BuildRequires:  pkgconfig(mpris-qt5)
BuildRequires:  mpv-libs-devel
BuildRequires:  pkgconfig(openssl)
BuildRequires:  pkgconfig(xtst)
BuildRequires:  pkgconfig(xcb-aux)
BuildRequires:  pkgconfig(xcb-ewmh)
BuildRequires:  pkgconfig(xcb-proto)
BuildRequires:  pkgconfig(xcb-shape)
BuildRequires:  libappstream-glib
BuildRequires:  gcc
BuildRequires:  desktop-file-utils

Requires:  dde-qt5integration
Requires:  dtkcore-devel
Requires:  dtkwidget-devel

%description
Deepin movie for deepin desktop environment.

%description -l zh_CN
深度影音播放器, 后端基于MPV, 支持解码大多数视频格式.

%package devel
Summary:        Development package for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Header files and libraries for %{name}.

%prep
%autosetup -p1 -n %{name}-reborn-%{version}
sed -i '/dtk2/s|lib|libexec|' src/CMakeLists.txt
sed -i '/#include <DPalette>/a #include <QPainterPath>' src/widgets/{tip,toolbutton}.h
sed -i 's/Exec=deepin-movie/Exec=env QT_QPA_PLATFORMTHEME=deepin deepin-movie/g' ./%{name}.desktop

%build
%cmake3 -DCMAKE_BUILD_TYPE=Release
%make_build

%install
%make_install
install -Dm644 %SOURCE1 %{buildroot}%{_datadir}/appdata/%{name}.appdata.xml
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/appdata/%{name}.appdata.xml
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop

rm -rf %{buildroot}/%{_datadir}/deepin-manual/

%find_lang %{name} --with-qt

%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{_libdir}/libdmr.so.0.1
%{_libdir}/libdmr.so.0.1.0
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/glib-2.0/schemas/com.deepin.%{name}.gschema.xml

%files devel
%{_includedir}/libdmr/
%{_libdir}/pkgconfig/libdmr.pc
%{_libdir}/libdmr.so

%changelog
* Wed Dec 07 2022 liweiganga <liweiganga@uniontech.com> - 5.7.11-1
- update: update to 5.7.11

* Mon Feb 07 2022 haomimi <haomimi@uniontech.com> - 3.2.24.3-2
- fix build error

* Tue Aug 03 2021 weidong <weidong@uniontech.com> - 3.2.24.3-1
- Init packages

